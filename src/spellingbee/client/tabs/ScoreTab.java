package spellingbee.client.tabs;

import javafx.scene.control.Tab;
import javafx.scene.layout.*;
import spellingbee.network.Client;

/**
 * Date 2020-12-28
 * This class contains everything about the scores (Did not do much as it was the job of obi-wan)
 * @author Sammy Pantaleon-Hernandez
 *
 */
public class ScoreTab extends Tab{
	private Client client;
	
	public ScoreTab(Client client) {
		super("Score");
		this.client = client;
		
		VBox overall = new VBox();
		
		this.setContent(overall);
	}
}
