package spellingbee.client.tabs.events;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import spellingbee.network.Client;
import spellingbee.client.tabs.*;
public class SubmitEvent extends GameTab implements EventHandler<ActionEvent>{
	private Client client;
	private TextField message;
	private TextField score;
	private TextField input;
	private Button[] buttons;
	
	/**
	 * @param client 
	 * @param message
	 * @param score
	 * @param input
	 * @param buttons
	 */
	public SubmitEvent(Client client, TextField message, TextField score, TextField input, Button[] buttons) {
		super(client);
		this.client = client;
		this.message = message;
		this.score = score;
		this.input = input;
		this.buttons = buttons;
	}
	
	
	/**
	 *Get the input of the user and send a string to the server
	 *With the answer received by the server, it shows the result and points the user currently have.
	 *Clears the input Textfield and tries to change to the next letters
	 *(Did not fully tested the next Letters part)
	 */
	public void handle(ActionEvent event) {
		String word = this.input.getText();
		String ServerAnswer = this.client.sendAndWaitMessage("getMessage();getScore();" + word);
		String[] answer = ServerAnswer.split(";");
		this.message.setText(answer[0]);
		this.score.setText(answer[1]);
		this.input.setText("");
		changeButtons(this.client, this.buttons); //This line was meant to get the next word
	}
}
