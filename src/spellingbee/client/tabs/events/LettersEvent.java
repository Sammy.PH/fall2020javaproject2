package spellingbee.client.tabs.events;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;

/**
 * Date 2020-12-28
 * This class contains everything about the spellingbee game from the client side
 * @author Sammy Pantaleon-Hernandez
 *
 */
public class LettersEvent implements EventHandler<ActionEvent>{
	/**
	 * TextField input of the user
	 */
	private TextField input;
	
	/**
	 * Button one of the 7 button that has been clicked
	 */
	private Button clicked;
	
	/**
	 * Constructor of the class
	 * @param input
	 * @param clicked
	 */
	public LettersEvent(TextField input, Button clicked) {
		this.input = input;
		this.clicked = clicked;
	}
	
	
	/**
	 *Get the value of the clicked button and adds it to the input textfield
	 */
	public void handle(ActionEvent event) {
		String letter = this.clicked.getText();
		String word = input.getText() + letter;
		input.setText(word);
	}
}
