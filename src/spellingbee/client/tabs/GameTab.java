package spellingbee.client.tabs;

import javafx.scene.control.*;
import javafx.scene.layout.*;
import spellingbee.network.Client;
import spellingbee.client.tabs.events.*;
/**
 * Date 2020-12-28
 * This class contains everything about the spellingbee game from the client side
 * @author Sammy Pantaleon-Hernandez
 *
 */
public class GameTab extends Tab{
	
	/**
	 * Client client connection
	 */
	private Client client;
	/**
	 * Button[] button array for all 7 buttons
	 */
	private Button[] buttons = new Button[7];
	/**
	 * TextField input of the user
	 */
	private TextField input;
	/**
	 * Button submit button
	 */
	private Button submit;
	/**
	 * message result message from the server
	 */
	private TextField message;
	/**
	 *score current score of the user
	 */
	private TextField score;
	
	/**
	 * This is the constructor of the class
	 * It creates every element for the game tab and assign it with an initial value
	 * @param client connection to the server
	 */
	public GameTab(Client client) {
		
		super("Game");
		this.client = client;
		VBox overall = new VBox();
		
		//Looking at server for the letters
		changeButtons(this.client, this.buttons);
		
		//Create button for the letters
		ButtonBar buttonBar = new ButtonBar();
		buttonBar.getButtons().addAll(buttons[0], buttons[1], buttons[2], buttons[3], buttons[4], buttons[5], buttons[6]);
		
		//Showing which letter has been clicked
		this.input = new TextField();
		
		//Submit answer
		this.submit = new Button("Submit");
		
		//Showing result and score
		HBox results = new HBox();
		this.message = new TextField();
		this.score = new TextField();
		
		//Adding everything and showing the tab content
		results.getChildren().addAll(message, score);
		overall.getChildren().addAll(buttonBar, input, submit, results);
		this.setContent(overall);
	}
	
	
	
	/**
	 * Method that is going to ask the server for the 7 letter buttons
	 * @param client connection
	 * @param buttons all character buttons
	 */
	public void changeButtons(Client client, Button[] buttons) {
		String letterString = client.sendAndWaitMessage("getAllLetters()");
		char[] lettersArray = letterString.toCharArray();
		String centerLetter = client.sendAndWaitMessage("getCenterLetter()");
		for(int i = 0; i < lettersArray.length; i++) {
			String letter = Character.toString(lettersArray[i]);
			if(letter.equals(centerLetter)) {
				buttons[i] = new Button(centerLetter);
				buttons[i].setStyle("-fx-background-color: #00ff00");
			}
			else {
			buttons[i] = new Button(letter);
			}
		}
	}
	
	/**
	 * This method is called from the main method of the client
	 * Its purpose is react to when a button is clicked
	 */
	public void events() {
		for(int i = 0; i < this.buttons.length; i++) {
			LettersEvent event = new LettersEvent(this.input, buttons[i]);
			buttons[i].setOnAction(event);
		}
		SubmitEvent submitting = new SubmitEvent(this.client, this.message, this.score, this.input, this.buttons);
		this.submit.setOnAction(submitting);
	}
}
