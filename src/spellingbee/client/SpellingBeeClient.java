package spellingbee.client;
import javafx.application.*;
import javafx.stage.*;
import javafx.scene.*;
import javafx.scene.paint.*;
import javafx.scene.control.*;
import spellingbee.client.tabs.*;

/**
 * Date 2020-12-28
 * This class is the application client
 * @author Sammy Pantaleon-Hernandez
 *
 */
public class SpellingBeeClient extends Application{
	/**
	 * ClientApp to get the connection to the server
	 */
	private ClientApp client = new ClientApp();
	
	/**
	 * Main method
	 * @param args
	 */
	public static void main(String[] args) {
		
		Application.launch(args);
	}
	
	/**
	 *Set the application
	 */
	public void start(Stage stage) {
		TabPane root = new TabPane();
		GameTab game = new GameTab(client.getClient());
		ScoreTab score = new ScoreTab(client.getClient());
		
		root.getTabs().addAll(game, score);
		game.events();
		
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.BLACK);
		
		stage.setTitle("Spelling bee"); 
		stage.setScene(scene); 
		
		stage.show();
	}

}
