package spellingbee.client;
import spellingbee.network.*;

/**
 * Date 2020-12-28
 * This class makes a connection with server
 * @author Sammy Pantaleon-Hernandez
 *
 */
public class ClientApp {
	/**
	 * Client client object
	 */
	private Client client;
	
	/**
	 * Constuctor of the class
	 * Assigns a new Client object to the client variable
	 */
	public ClientApp() {
		this.client = new Client();
	}
	
	/**
	 * Get method to access the client object from another class
	 * @return client 
	 */
	public Client getClient() {
		return this.client;
	}
}
