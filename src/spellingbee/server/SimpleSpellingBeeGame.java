package spellingbee.server;

import java.util.*;

/**
 * Date 2020-12-28
 * This class hold all information regarding the spellingbee game
 * It is on the server side
 * @author Sammy Pantaleon-Hernandez
 *
 */
public class SimpleSpellingBeeGame implements ISpellingBeeGame{
	/**
	 * String letters given to make the words
	 */
	private String letters;
	/**
	 * char mandatory word
	 */
	private char center;
	/**
	 * int number of points the user has
	 */
	private int points = 0;
	private final String[] wordsArray = new String[] { "estival", "livest", "valets", "vitals",  "valet", "vitas", "vast", "vest", "vets"};
	/**
	 * set<String> list of words accepted
	 */
	private final Set<String> words = new HashSet<>(Arrays.asList(wordsArray));
	
	/**
	 * Constructor
	 */
	public SimpleSpellingBeeGame() {
		this.letters = "lseiav";
		this.center = 't';
	}
	
	

	/**
	 * This method returns how many points the user needs to have in order to rank higher
	 * @return ranks
	 */
	public int[] getBrackets() {
		int[] ranks = new int[]{1, 3, 5, 6, 10};
		return ranks;
	}
	
	
	/**
	 * The method takes the length of the attempt and show the correct amount of point the attempt is worth
	 *@param attempt of user
	 *@return points 
	 */
	public int getPointsForWord(String attempt) {
		if(attempt.length() == 4) {
			this.points += 1;
			return 1;
		}
		else if(attempt.length() > 4 && attempt.length() <7) {
			this.points += attempt.length();
			return attempt.length();
		}
		else if(attempt.length() == 7) {
			this.points += (attempt.length() + 7);
			return attempt.length() + 7;
		}
		return 0;
	}
	
	
	/**
	 *This method takes the attempt string, split it into an array, make the array a list to figue out
	 *if the list contains the center letter. It also looks if the attempt string is part of the accepted words
	 *calls the getPointsForWord method to assign the points
	 *@param attempt of the user
	 *@return message if it a valid or invalid input
	 */
	public String getMessage(String attempt) {
		String[] StringArray = attempt.split("");
		Set<String> StringList = new HashSet<>(Arrays.asList(StringArray));
		if(StringList.contains(Character.toString(this.center)) && this.words.contains(attempt)) {
			if(getPointsForWord(attempt) > 1) {
				return "good";
			}
			return "wrong";
		}
		else {
			return "wrong";
		}
	}
	
	/**
	 * This method returns the letters necessary
	 * @return allLeters 
	 */
	public String getAllLetters() {
		return this.letters + this.center;
	}
	
	/**
	 * This method returns the mandatory letter
	 * @return centerLetter
	 */
	public char getCenterLetter() {
		return this.center;
	}
	
	/**
	 * This method returns the current score of the user
	 * @return score
	 */
	public int getScore() {
		return points;
	}
}
