package spellingbee.server;

/**
 * Date 2020-12-28
 * This interface is for useful to switch betwenn spellingbee games
 * @author Sammy Pantaleon-Hernandez
 *
 */
public interface ISpellingBeeGame {
	
	
	/**
	 * This method takes as an input the attempt of the user and returns the points the attempt is worth
	 * @param attempt
	 * @return the points the word is worth
	 */
	int getPointsForWord(String attempt);
	
	/**
	 * This method takes the attempt of the user, validates it and return a message to the user
	 * @param attempt attempt of user
	 * @return message result to user
	 */
	String getMessage(String attempt);
	
	/**
	 * This method returns the letters necessary
	 * @return allLeters 
	 */
	String getAllLetters() ;
	
	/**
	 * This method returns the mandatory letter
	 * @return centerLetter
	 */
	char getCenterLetter();
	
	/**
	 * This method returns the current score of the user
	 * @return score
	 */
	int getScore();
	
	/**
	 * This method returns how many points the user needs to have in order to rank higher
	 * @return ranks
	 */
	int[] getBrackets();
}
